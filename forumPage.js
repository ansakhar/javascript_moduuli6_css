//import { v4 as uuidv4 } from "uuid";

function toggleAddPost() {
    const x = document.getElementById("addPostForm");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
    console.log("toggle button clicked");
}

function removePost(postId) {
    const removingPost = document.getElementById(postId);
    removingPost.remove();
    //console.log(`removing...${postId}`);
}

function addPost() {
   
    const userName = document.getElementById("uname").value;
    const newPost = document.getElementById("upost").value;
    if ((userName === "")||(newPost === "")) {
        window.alert("error: name or post are empty");
        return;
    }
    const posts = document.getElementById("posts");
    const li = document. createElement("li");

    const id_attr = document.createAttribute("id");
    const postId = Math.floor(Math.random() * 10000);
    id_attr.value= postId;
    li.setAttributeNode(id_attr);

    const class_attr = document.createAttribute("class");
    class_attr.value = "post_elem";
    li.setAttributeNode(class_attr);

    const flexBox = document.createElement("div");

    const deleteButton = document.createElement("button");
    const class_button = document.createAttribute("class");
    class_button.value = "delete_button";
    deleteButton.setAttributeNode(class_button);
    deleteButton.innerHTML = "Delete";
    deleteButton.onclick = function() { removePost(postId); };
    flexBox.appendChild(deleteButton);

    const hName = document. createElement("h3");
    hName. appendChild(document. createTextNode(userName));
    flexBox. appendChild(hName);

    const pPost = document. createElement("div");
    pPost. appendChild(document. createTextNode(newPost));
    li.appendChild(flexBox);
    li. appendChild(pPost);

    posts. appendChild(li);

    document.getElementById("uname").value = "";
    document.getElementById("upost").value = "";
}
